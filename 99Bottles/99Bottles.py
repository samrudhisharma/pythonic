# 99Bottles.py
# Python 2.7.6
# samrudhisharma
"""
Milestones:
- setup basic poem structure
- handle case: 1 bottle 
"""
def poem():
  end  = 99
  start = -1
  while end > start:
    if end == 1:
      print str(end) + " bottle of beer on the wall, "+str(end)+" bottle of beer.\nTake one down and pass it around, no more bottles of beer on the wall."
    elif end == 0:
      print "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall."
    else:
      print str(end) + " bottles of beer on the wall, "+str(end)+" bottles of beer.\nTake one down and pass it around, "+str(end-1) +" bottles of beer on the wall."
    end-=1
    
def main():
  poem()
if __name__ == '__main__':
  main()
